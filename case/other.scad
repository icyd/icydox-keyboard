use <NopSCADlib/utils/tube.scad>
include <NopSCADlib/core.scad>
use <NopSCADlib/vitamins/pcb.scad>

module copy_mirror(vec=[0,1,0])
{
    children();
    mirror(vec) children();
}

module jack(cutout = false)
{
    l = 13.35;
    w = 6;
    h = 5;
    d = 5.5;
    ch = 1.2;

    translate_z(h / 2)
        if(cutout)
            rotate([0, 90, 0])
                cylinder(d = d + 2 * panel_clearance, h = 100);
        else
            color(grey(20))
                rotate([0, 90, 0]) {
                    linear_extrude(l / 2)
                        difference() {
                            square([h, w], center = true);

                            circle(d = 3.5);
                        }

                    translate_z(l/2) tube(or  = d / 2, ir = 3.5 / 2, h = ch, center = false);

                    translate_z(-l / 4)
                        cube([h, w, l / 2], center = true);
                }
}

module arduino_pro()
{
    color("DarkBlue") translate([0, 0, 0.8+2.5]) cube([18.2, 33.2, 1.6], center=true);
    copy_mirror([1, 0, 0]) color("Black") translate([7.5, -2.7/2, 1.25]) cube([2.5, 30.5, 2.5], center=true);
    translate([0, 33.2/2-2, 2.5+1.6])rotate([0, 0, 90]) usb_uA();
}


