use <knurledFinishLib_v2.scad>

module encoder(base_w=12,
					 base_l=15.3,
					 base_h=6.5,
					 thread_d=7,
					 thread_h=7,
					 shaft_d=6,
					 shaft_h=12.5,
					 cut_h=10,
					 cut_d=0,
               cap_d=16,
               cap_undercut=1,
					 sw_travel=0.5,
					 for_diff=false,
					 tolerance=1) {
	$fs=0.4;
	cf=0.1/100;
   ovr_size=for_diff ? tolerance : 0;
	union() {
		translate([0, 0, (base_h+ovr_size)/2]) color("SteelBlue") cube([base_w+ovr_size, base_l+ovr_size, base_h+ovr_size*(1+cf)], center=true);
		color("Silver") union() {
			translate([0, 0, base_h]) cylinder(thread_h, d=thread_d+ovr_size);
   	   if (!for_diff) {
				translate([0, 0, base_h+thread_h]) difference() {
					cylinder(shaft_h+sw_travel, d=shaft_d+ovr_size);
					translate([-shaft_d/2, (shaft_d-cut_d), shaft_h+sw_travel-cut_h]) cube([shaft_d, shaft_d/2, cut_h*(1+cf)]);
				}
			} else {
				translate([0, 0, base_h+thread_h-cap_undercut-sw_travel]) cylinder(shaft_h+cap_undercut+sw_travel, d=cap_d+ovr_size);
			}
		}
	}
}

module encoder_d(base_w=12,
					 base_l=15.3,
					 base_h=6.5,
					 thread_d=7,
					 thread_h=7,
					 shaft_d=6,
					 shaft_h=12.5,
					 cut_h=10,
					 cut_d=4.5,
               cap_d=12,
               cap_undercut=1,
               sw_travel=0.5,
               for_diff=false,
					 tolerance=1) {
	encoder(base_w,
			   base_l,
				base_h,
				thread_d,
				thread_h,
				shaft_d,
				shaft_h,
				cut_h,
				cut_d,
				cap_d,
           cap_undercut,
           sw_travel,
				for_diff,
				tolerance);
}
//encoder_d();
module cap(shaft_d=6,
				shaft_h=12.5,
				cut_h=10,
				cut_d=4.5,
				sw_travel=0.5,
				cap_d=16,
        cap_d2=12,
				cap_h=15,
				thread_d=7,
				tol_perc=5,
				cap_undercut=1.5,
				knurled=false) {
	$fs=0.4;
	cf=0.1/100;
	tol=1+tol_perc/100;
	color("DimGray") difference() {
		union() {
			translate([0, 0, -cap_undercut]) cylinder(cap_undercut, d=cap_d2);
			if (knurled) {
				knurl(k_cyl_hg=cap_h,
		 				 k_cyl_od=cap_d,
	     				 knurl_wd=3,
   	 				 knurl_hg=4,
			         knurl_dp=1.5,
		 				 e_smooth=2,
						 s_smooth=50);
			} else {
				cylinder(cap_h, d=cap_d);
			}
		}
		union() {
			translate([0, 0, -cf/2-cap_undercut]) cylinder(cap_undercut*(1+cf), d=thread_d*tol);
			difference() {
				cylinder(shaft_h+sw_travel, d=shaft_d*tol);
				translate([-shaft_d/2, shaft_d*tol-cut_d, shaft_h+sw_travel-cut_h]) cube([shaft_d, shaft_d/2, cut_h*(1+cf)]);
			}
		}
	}
}
cap();
