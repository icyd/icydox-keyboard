include <kle/icydox-layout.scad>
include <scad-keyboard-cases/keyboard-case.scad>

use <EC11-rot_encoder.scad>
use <NopSCADlib/vitamins/button.scad>
use <other.scad>
use <scad-keyboard-cases/Lenbok_Utils/utils.scad>
use <scad-keyboard-cases/Lenbok_Utils/bezier.scad>

top_case_raised_height = 6.6 + 2; // Distance between plate and bottom of keycap plus a little extra, for raised top case
bottom_case_height = 15;  // Enough room to house electonics
depth_offset = 0;       // How much of side wall to include below top plate

pcb_thickness=1.6;

// Case screw sizes
screw_head_rad = 5.3 / 2;
screw_rad = 3.0 / 2;
screw_length = 8;

$fa = 1;
$fs = $preview ? 5 : 2;
bezier_precision = $preview ? 0.05 : 0.025;

// Hacky way to select just the left hand keys from split iris/redox layout
left_keys = [ for (i = icydox_layout) if (key_pos(i).x < 8) i ];

jack_hole = 8;
jack_h = 5;
usb_hole = [13, 9];
usb_offset = 2.5 + 1.6 + 2.4/2;

origin = [56.5658, -37.3825];
orig_reference_points = [
	[65.888, 139.725],
	[65.888, 125.501],
	[61.062, 120.675],
	[61.062, 82.677],
	[65.888, 78.511],
	[65.888, 41.173],
	[70.968, 36.093],
	[194.285, 36.093],
	[199.365, 41.173],
	[199.365, 140.716],
	[208.763, 146.075],
	[199.238, 162.585],
	//[159.5, 139.725],
	[157.709, 138.709],
	[135.738, 132.867],
	[125.578, 132.867],
	[123.578, 135.407],
	[108.306, 135.407],
	[103.988, 139.725],
];

bezier_points = [
	[65.87, 36.08], // Top Left
	[132.62, 36.08], // Top
	[199.36, 36.08], // Top Right
	[199.364, 110], // Right
	[208.763, 146.075], // Right Bottom
	[199.111, 162.585], // Bottom Right
	[157.709, 138.709], // Bottom 1
	[103.988, 139.725], // Bottom 2
	[65.87, 139.7254], // Bottom Left
	[61.062, 101.676], // Left
];

orig_screw_holes = [
	[85.1408, 63.5762],
	[161.3408, 60.0837],
	[191.3128, 129.286],
	[83.8708, 120.7262],
];

orig_tent_positions = [
	/*[[199.365, 50], 0],
		[[199.365, 130], 0],*/
];

orig_jack_pos = [189.23, 36.068];
orig_reset_pos = [94.7408, 40.7818];
orig_micro_pos = [132.7658, 53.4162];
orig_encoder_pos = [189.9158, 108.6612];

// ##########################################
// Adjust all points with reference to origin
// ##########################################
reference_points = adj(orig_reference_points, origin);
bz_p = adj(bezier_points, origin);
screw_holes = adj(orig_screw_holes, origin);
tent_positions = adj(orig_tent_positions, origin);
jack_pos = adj([orig_jack_pos], origin)[0];
reset_pos = adj([orig_reset_pos], origin)[0];
micro_pos = adj([orig_micro_pos], origin)[0];
encoder_pos = adj([orig_encoder_pos], origin)[0];

bzVec = [
	bz_p[0] + [-2, -2], POLAR(20, 4),
	OFFSET([-35, -2]), bz_p[1] + [-10, 4], POLAR(15, 4),
	POLAR(25, 150), bz_p[2] + [0, -2], POLAR(10, -60),
	POLAR(25, 85), bz_p[3], POLAR(10, -85),
	POLAR(12, 150), bz_p[4], SHARP(),
	SYMMETRIC(), bz_p[5], SHARP(),
	POLAR(10, -25), bz_p[6] + [0, -3], POLAR(20, 160),
	POLAR(30, 15), bz_p[7], POLAR(22, 190),
	POLAR(10, -8), bz_p[8] + [-2, -2], POLAR(18, 100),
	POLAR(27, -94), bz_p[9] + [-2, 0], POLAR(40, 82),
	POLAR(20, -80), bz_p[0] + [-2, -2],
];

function adj(points, origin=[0, 0]) = [for (point = points) point * [[1, 0],[0,-1]] - origin];

module rev0_outer_profile(bezier=false) {
	fillet(r = 5, $fn = 20)
		offset(r = 5, chamfer = false)
		if (!bezier) {
			polygon(points = reference_points, convexity = 3);
		} else {
			polygon(Bezier(bzVec, precision = bezier_precision));
		}
}

module rev0_top_case(raised=false, bezier=false) {
	z_offset = 5 - plate_thickness + pcb_thickness + depth_offset;
	difference() {
		top_case(left_keys, [for (i = screw_holes) i], raised=raised) rev0_outer_profile(bezier);
		translate(concat(encoder_pos, -5+plate_thickness)) encoder_d(cap_d=12, cap_undercut=2, for_diff=true);
		translate([jack_pos[0], 10, -z_offset-jack_h/2]) rotate([90, 0, 0]) polyhole(r=jack_hole/2, h=20, center=true);
		translate([micro_pos[0], 10, -z_offset-usb_offset]) rotate([90, 0, 0]) roundedcube(concat(usb_hole, 20), r=2, center=true, $fs=1);
		for (s = screw_holes) {
			translate(concat(s, plate_thickness-1)) polyhole(r=screw_head_rad, h=1, center=false);
		}
	}
}

module rev0_bottom_case(bezier=false) {
	z_offset = -bottom_case_height+pcb_thickness+5-plate_thickness;
	difference() {
		bottom_case([for (i = screw_holes) i], [for (i = tent_positions) [i[0] * [[1, 0], [0, -1]] - origin + [4, 0], i[1]]]) rev0_outer_profile(bezier);
		translate(reset_pos) polyhole(r=2.5, h=2*wall_thickness+0.02, center=true);
		translate([jack_pos[0], 10, -z_offset-jack_h/2]) rotate([90, 0, 0]) polyhole(r=jack_hole/2, h=20, center=true);
		translate([micro_pos[0], 10, -z_offset-usb_offset]) rotate([90, 0, 0]) roundedcube(concat(usb_hole, 20), r=2, center=true, $fs=1);
		translate([0, 0, -z_offset]) linear_extrude(height = pcb_thickness+5-plate_thickness+1, center=false, convexity=3) offset(r=-8, chamfer=false) rev0_outer_profile(bezier);
	}
}

module pcb_edge() {
  color("DarkGreen") translate([0, 0, -0.01]) linear_extrude(height = pcb_thickness+0.01, center = false, convexity = 3)
	difference() {
		polygon(points = [for (i = reference_points) i], convexity = 3);
		for (pos = screw_holes) {
			translate(pos) {
				polyhole2d(r = 3.2/2);
			}
		}
	}
}

module pcb() {
	pcb_edge();
	translate(reset_pos) rotate([0, 180, 0]) square_button(["button_6mm", 6.0, 3.0, 0.2, 1.0, 3.5, 4.2, 0]);
	translate(jack_pos - [0, 6]) rotate([0, 180, -90]) jack();
	translate(micro_pos) rotate([0, 180, 0]) arduino_pro();

}

part = "top";
bezier=true;
explode = 0;
if (part == "bezier") {
	BezierVisualize(bzVec);
} else if (part == "outer") {
	pcb_edge();
	rev0_outer_profile(bezier);
	for (pos = screw_holes) {
		translate(pos) {
			polyhole2d(r = 3.2 / 2);
		}
	}
#key_holes(left_keys);
} else if (part == "top") {
	rev0_top_case(true, bezier);

} else if (part == "bottom") {
	rev0_bottom_case(bezier);

} else if (part == "assembly") {
	%translate([0, 0, plate_thickness + 30 * explode]) key_holes(left_keys, "keycap");
	%translate([0, 0, plate_thickness + 20 * explode]) key_holes(left_keys, "switch");
	translate(concat(encoder_pos, -1 + 20 * explode)) encoder_d();
	translate(concat(encoder_pos, 13.5-1 + 30 * explode)) %cap();
	rev0_top_case(true, bezier);
	translate([0, 0, -bottom_case_height -20 * explode]) rev0_bottom_case(bezier);
} else if (part == "topnpcb") {
	rev0_top_case(true);
	translate([0, 0, -5+plate_thickness-pcb_thickness]) pcb();
	translate([0, 0, plate_thickness]) key_holes(left_keys, "switch");
} else if (part == "bottomnpcb") {
	rev0_bottom_case(bezier);
	translate([0, 0, bottom_case_height-pcb_thickness-5+plate_thickness]) pcb();
} else if (part == "casenpcb") {
	rev0_top_case(true, bezier);
	translate([0, 0, -pcb_thickness-5+plate_thickness]) pcb();
	translate([0, 0, -bottom_case_height]) rev0_bottom_case(bezier);
} else if (part == "case") {
	rev0_top_case(true, bezier);
	translate([0, 0, -bottom_case_height]) rev0_bottom_case(bezier);
} else if (part == "cap") {
	cap(knurled=true);
}

