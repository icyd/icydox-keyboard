include <kle/icydox-layout.scad>
include <scad-keyboard-cases/keyboard-case.scad>
use <EC11-rot_encoder.scad>
use <NopSCADlib/vitamins/button.scad>
use <other.scad>

// Requires my utility functions in your OpenSCAD lib or as local submodule
// https://github.com/Lenbok/scad-lenbok-utils.git
use <scad-keyboard-cases/Lenbok_Utils/utils.scad>
// Requires bezier library from https://www.thingiverse.com/thing:2207518
// use <scad-keyboard-cases/Lenbok_Utils/bezier.scad>

$fa = 1;
$fs = $preview ? 5 : 2;

// Hacky way to select just the left hand keys from split iris/redox layout
left_keys = [ for (i = icydox_layout) if (key_pos(i).x < 8) i ];

jack_hole = 8;
jack_h = 5;
usb_hole = [13, 9];
usb_offset = 2.5 + 1.6 + 2.4/2;

origin = [56.5658, -37.3825];
orig_reference_points = [
	[65.888, 139.725],
	[65.888, 125.501],
	[61.062, 120.675],
	[61.062, 82.677],
	[65.888, 78.511],
	[65.888, 41.173],
	[70.968, 36.093],
	[194.285, 36.093],
	[199.365, 41.173],
	[199.365, 140.716],
	[208.763, 146.075],
	[199.238, 162.585],
	//[159.5, 139.725],
	[157.709, 138.709],
	[135.738, 132.867],
	[125.578, 132.867],
	[123.578, 135.407],
	[108.306, 135.407],
	[103.988, 139.725],
];

orig_screw_holes = [
	[85.1408, 48.26],
	[85.1408, 123.444],
	[191.3128, 129.286],
	[161.3408, 48.768],
];

orig_tent_positions = [
	/*[[199.365, 50], 0],
		[[199.365, 130], 0],*/
];

orig_jack_pos = [189.23, 36.068];
orig_reset_pos = [94.7408, 40.7818];
orig_micro_pos = [132.7658, 53.4162];
orig_encoder_pos = [189.9158, 108.6612];

// ##########################################
// Adjust all points with reference to origin
// ##########################################
reference_points = adj(orig_reference_points, origin);
screw_holes = adj(orig_screw_holes, origin);
tent_positions = adj(orig_tent_positions, origin);
jack_pos = adj([orig_jack_pos], origin)[0];
reset_pos = adj([orig_reset_pos], origin)[0];
micro_pos = adj([orig_micro_pos], origin)[0];
encoder_pos = adj([orig_encoder_pos], origin)[0];

function adj(points, origin=[0, 0]) = [for (point = points) point * [[1, 0],[0,-1]] - origin];

module rev0_outer_profile() {
	fillet(r = 5, $fn = 20)
		offset(r = 5, chamfer = false)
		polygon(points = reference_points, convexity = 3);
}

module rev0_top_case(raised=false) {
	difference() {
		top_case(left_keys, [for (i = screw_holes) i], raised=raised) rev0_outer_profile();
		translate(encoder_pos) encoder_d(cap_d=12, cap_undercut=2, for_diff=true);
		translate([jack_pos[0], 5, -depth_offset-jack_h/2]) rotate([90, 0, 0]) polyhole(r=jack_hole/2, h=10, center=true);
		translate([micro_pos[0], 5, -depth_offset-usb_offset]) rotate([90, 0, 0]) roundedcube(concat(usb_hole, 10), r=2, center=true, $fs=1);
	}
}

module rev0_bottom_case() {
	difference() {
		bottom_case([for (i = screw_holes) i], [for (i = tent_positions) [i[0] * [[1, 0], [0, -1]] - origin + [4, 0], i[1]]]) rev0_outer_profile();
		translate(reset_pos) polyhole(r=2.5, h=2*wall_thickness+0.02, center=true);
		translate([jack_pos[0], 5, bottom_case_height-depth_offset-jack_h/2]) rotate([90, 0, 0]) polyhole(r=jack_hole/2, h=10, center=true);
		translate([micro_pos[0], 5, bottom_case_height-depth_offset-usb_offset]) rotate([90, 0, 0]) roundedcube(concat(usb_hole, 10), r=2, center=true, $fs=1);
	}
}

module pcb_edge() {
  color("DarkGreen") translate([0, 0, -0.01]) linear_extrude(height = 1.6+0.01, center = false, convexity = 3)
	difference() {
		polygon(points = [for (i = reference_points) i], convexity = 3);
		for (pos = screw_holes) {
			translate(pos) {
				polyhole2d(r = 3.2/2);
			}
		}
	}
}

module pcb() {
	pcb_edge();
	translate(reset_pos) rotate([0, 180, 0]) square_button(["button_6mm", 6.0, 3.0, 0.2, 1.0, 3.5, 4.2, 0]);
	translate(jack_pos - [0, 6]) rotate([0, 180, -90]) jack();
	translate(micro_pos) rotate([0, 180, 0]) arduino_pro();

}

part = "top";
explode = 1;
if (part == "outer") {
	pcb_edge();
	rev0_outer_profile();
	for (pos = screw_holes) {
		translate(pos) {
			polyhole2d(r = 3.2 / 2);
		}
	}
#key_holes(left_keys);
} else if (part == "top") {
	rev0_top_case(true);

} else if (part == "bottom") {
	rev0_bottom_case();

} else if (part == "assembly") {
	%translate([0, 0, plate_thickness + 30 * explode]) key_holes(left_keys, "keycap");
	%translate([0, 0, plate_thickness + 20 * explode]) key_holes(left_keys, "switch");
	translate(concat(encoder_pos, -1 + 20 * explode)) encoder_d();
	translate(concat(encoder_pos, 13.5-1 + 30 * explode)) %cap();
	rev0_top_case();
	translate([0, 0, -bottom_case_height -20 * explode]) rev0_bottom_case();
} else if (part == "topnpcb") {
	rev0_top_case(true);
	translate([0, 0, -1.6]) pcb();
} else if (part == "bottomnpcb") {
	translate([0, 0, -bottom_case_height]) rev0_bottom_case();
	translate([0, 0, -1.6]) pcb();
} else if (part == "casenpcb") {
	rev0_top_case(true);
	translate([0, 0, -bottom_case_height]) rev0_bottom_case();
	translate([0, 0, -1.6]) pcb();
} else if (part == "cap") {
	cap(knurled=true);
}

