EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Icydox keyboard - Redox mod"
Date "2020-12-15"
Rev "1.0"
Comp ""
Comment1 "https://github.com/mattdibi/redox-keyboard"
Comment2 "Based on redox-keyboard"
Comment3 "https://github.com/icyd/icydox-keyboard"
Comment4 "Designed by Alberto Vázquez"
$EndDescr
$Comp
L icydox:ProMicro U1
U 1 1 5A8086FE
P 2350 2150
F 0 "U1" H 2350 2150 60  0000 C CNN
F 1 "ProMicro" H 2350 1400 60  0000 C CNN
F 2 "Keebio-Parts:ArduinoProMicro" V 3400 -350 60  0001 C CNN
F 3 "" V 3400 -350 60  0001 C CNN
	1    2350 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5A80895C
P 3050 1700
F 0 "#PWR01" H 3050 1450 50  0001 C CNN
F 1 "GND" H 3050 1550 50  0000 C CNN
F 2 "" H 3050 1700 50  0001 C CNN
F 3 "" H 3050 1700 50  0001 C CNN
	1    3050 1700
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 5A808978
P 3050 1900
F 0 "#PWR04" H 3050 1750 50  0001 C CNN
F 1 "VCC" H 3050 2050 50  0000 C CNN
F 2 "" H 3050 1900 50  0001 C CNN
F 3 "" H 3050 1900 50  0001 C CNN
	1    3050 1900
	0    1    1    0   
$EndComp
Text GLabel 3300 1800 2    60   Input ~ 0
RST
Wire Wire Line
	3050 1800 3300 1800
Text GLabel 1450 2100 0    60   Input ~ 0
SCL
Text GLabel 1450 2000 0    60   Input ~ 0
SDA
Wire Wire Line
	1450 2000 1650 2000
Wire Wire Line
	1450 2100 1650 2100
Text GLabel 3300 2100 2    60   Input ~ 0
col3
Text GLabel 3300 2200 2    60   Input ~ 0
col4
Text GLabel 3300 2300 2    60   Input ~ 0
col5
Text GLabel 3300 2400 2    60   Input ~ 0
col6
Text GLabel 3300 2500 2    60   Input ~ 0
row4
Text GLabel 3300 2600 2    60   Input ~ 0
row3
Text GLabel 3300 2700 2    60   Input ~ 0
row2
Wire Wire Line
	3050 2100 3300 2100
Wire Wire Line
	3050 2200 3300 2200
Wire Wire Line
	3050 2300 3300 2300
Wire Wire Line
	3050 2400 3300 2400
Wire Wire Line
	3050 2500 3300 2500
Wire Wire Line
	3050 2600 3300 2600
Wire Wire Line
	3050 2700 3300 2700
Text GLabel 1450 2200 0    60   Input ~ 0
row0
Text GLabel 1450 2400 0    60   Input ~ 0
col0
Text GLabel 1450 2500 0    60   Input ~ 0
col1
Text GLabel 1450 2600 0    60   Input ~ 0
col2
Text GLabel 1450 2700 0    60   Input ~ 0
row1
Wire Wire Line
	1450 2400 1650 2400
Wire Wire Line
	1450 2500 1650 2500
Wire Wire Line
	1450 2600 1650 2600
Wire Wire Line
	1450 2700 1650 2700
Wire Wire Line
	1450 2200 1650 2200
NoConn ~ 3050 1600
Wire Wire Line
	1450 1600 1650 1600
$Comp
L power:GND #PWR05
U 1 1 5A808DAF
P 1430 3630
F 0 "#PWR05" H 1430 3380 50  0001 C CNN
F 1 "GND" H 1430 3480 50  0000 C CNN
F 2 "" H 1430 3630 50  0001 C CNN
F 3 "" H 1430 3630 50  0001 C CNN
	1    1430 3630
	1    0    0    -1  
$EndComp
Text GLabel 1850 3450 2    60   Input ~ 0
SDA
Text GLabel 1850 3550 2    60   Input ~ 0
SCL
Text GLabel 1360 4530 0    60   Input ~ 0
RST
$Comp
L power:GND #PWR08
U 1 1 5A80901B
P 2150 4530
F 0 "#PWR08" H 2150 4280 50  0001 C CNN
F 1 "GND" H 2150 4380 50  0000 C CNN
F 2 "" H 2150 4530 50  0001 C CNN
F 3 "" H 2150 4530 50  0001 C CNN
	1    2150 4530
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5A8090D7
P 1650 1800
F 0 "#PWR02" H 1650 1550 50  0001 C CNN
F 1 "GND" H 1650 1650 50  0000 C CNN
F 2 "" H 1650 1800 50  0001 C CNN
F 3 "" H 1650 1800 50  0001 C CNN
	1    1650 1800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5A8090EE
P 1650 1900
F 0 "#PWR03" H 1650 1650 50  0001 C CNN
F 1 "GND" H 1650 1750 50  0000 C CNN
F 2 "" H 1650 1900 50  0001 C CNN
F 3 "" H 1650 1900 50  0001 C CNN
	1    1650 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 1500 4600 1600
$Comp
L icydox:KEYSW_w_LED K2
U 1 1 5A809089
P 5700 1500
F 0 "K2" H 5650 1500 60  0000 C CNN
F 1 "KEYSW" H 5700 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 5700 1500 60  0001 C CNN
F 3 "" H 5700 1500 60  0000 C CNN
	1    5700 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 1500 5400 1600
$Comp
L icydox:KEYSW_w_LED K3
U 1 1 5A8091F6
P 6500 1500
F 0 "K3" H 6450 1500 60  0000 C CNN
F 1 "KEYSW" H 6500 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 6500 1500 60  0001 C CNN
F 3 "" H 6500 1500 60  0000 C CNN
	1    6500 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 1500 6200 1600
$Comp
L icydox:KEYSW_w_LED K4
U 1 1 5A809203
P 7300 1500
F 0 "K4" H 7250 1500 60  0000 C CNN
F 1 "KEYSW" H 7300 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 7300 1500 60  0001 C CNN
F 3 "" H 7300 1500 60  0000 C CNN
	1    7300 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1500 7000 1600
$Comp
L icydox:KEYSW_w_LED K5
U 1 1 5A80948D
P 8100 1500
F 0 "K5" H 8050 1500 60  0000 C CNN
F 1 "KEYSW" H 8100 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8100 1500 60  0001 C CNN
F 3 "" H 8100 1500 60  0000 C CNN
	1    8100 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 1500 7800 1600
$Comp
L icydox:KEYSW_w_LED K6
U 1 1 5A80949A
P 8900 1500
F 0 "K6" H 8850 1500 60  0000 C CNN
F 1 "KEYSW" H 8900 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8900 1500 60  0001 C CNN
F 3 "" H 8900 1500 60  0000 C CNN
	1    8900 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 1500 8600 1600
$Comp
L icydox:KEYSW_w_LED K7
U 1 1 5A8094A7
P 9700 1500
F 0 "K7" H 9650 1500 60  0000 C CNN
F 1 "KEYSW" H 9700 1400 60  0001 C CNN
F 2 "redox_footprints:Mx_Alps_100-dualside" H 9700 1500 60  0001 C CNN
F 3 "" H 9700 1500 60  0000 C CNN
	1    9700 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 1500 9400 1600
$Comp
L Device:D D8
U 1 1 5A809C23
P 4600 2500
F 0 "D8" H 4600 2600 50  0000 C CNN
F 1 "D" H 4600 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 4600 2500 50  0001 C CNN
F 3 "" H 4600 2500 50  0001 C CNN
	1    4600 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 2250 4600 2350
$Comp
L Device:D D9
U 1 1 5A809C30
P 5400 2500
F 0 "D9" H 5400 2600 50  0000 C CNN
F 1 "D" H 5400 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 5400 2500 50  0001 C CNN
F 3 "" H 5400 2500 50  0001 C CNN
	1    5400 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5400 2250 5400 2350
$Comp
L Device:D D10
U 1 1 5A809C3D
P 6200 2500
F 0 "D10" H 6200 2600 50  0000 C CNN
F 1 "D" H 6200 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 6200 2500 50  0001 C CNN
F 3 "" H 6200 2500 50  0001 C CNN
	1    6200 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 2250 6200 2350
$Comp
L Device:D D11
U 1 1 5A809C4A
P 7000 2500
F 0 "D11" H 7000 2600 50  0000 C CNN
F 1 "D" H 7000 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7000 2500 50  0001 C CNN
F 3 "" H 7000 2500 50  0001 C CNN
	1    7000 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 2250 7000 2350
$Comp
L Device:D D12
U 1 1 5A809C57
P 7800 2500
F 0 "D12" H 7800 2600 50  0000 C CNN
F 1 "D" H 7800 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7800 2500 50  0001 C CNN
F 3 "" H 7800 2500 50  0001 C CNN
	1    7800 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7800 2250 7800 2350
$Comp
L Device:D D13
U 1 1 5A809C64
P 8600 2500
F 0 "D13" H 8600 2600 50  0000 C CNN
F 1 "D" H 8600 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 8600 2500 50  0001 C CNN
F 3 "" H 8600 2500 50  0001 C CNN
	1    8600 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8600 2250 8600 2350
$Comp
L Device:D D14
U 1 1 5A809C71
P 9400 2500
F 0 "D14" H 9400 2600 50  0000 C CNN
F 1 "D" H 9400 2400 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 9400 2500 50  0001 C CNN
F 3 "" H 9400 2500 50  0001 C CNN
	1    9400 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 2250 9400 2350
Wire Wire Line
	4500 2650 4600 2650
Connection ~ 5400 2650
Connection ~ 6200 2650
Connection ~ 7000 2650
Connection ~ 7800 2650
Connection ~ 8600 2650
Connection ~ 4600 2650
Text GLabel 4500 2650 0    60   Input ~ 0
row1
Text GLabel 5200 1300 1    60   Input ~ 0
col0
Text GLabel 6000 1300 1    60   Input ~ 0
col1
Text GLabel 6800 1300 1    60   Input ~ 0
col2
Text GLabel 7600 1300 1    60   Input ~ 0
col3
Text GLabel 8400 1300 1    60   Input ~ 0
col4
Text GLabel 9200 1350 1    60   Input ~ 0
col5
Text GLabel 10000 1350 1    60   Input ~ 0
col6
$Comp
L Device:R R1
U 1 1 5A80A2DF
P 1760 3260
F 0 "R1" V 1840 3260 50  0000 C CNN
F 1 "4.7k" V 1760 3260 50  0000 C CNN
F 2 "Keebio-Parts:Resistor-Compact" V 1690 3260 50  0001 C CNN
F 3 "" H 1760 3260 50  0001 C CNN
	1    1760 3260
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5A80A522
P 1560 3260
F 0 "R2" V 1640 3260 50  0000 C CNN
F 1 "4.7k" V 1560 3260 50  0000 C CNN
F 2 "Keebio-Parts:Resistor-Compact" V 1490 3260 50  0001 C CNN
F 3 "" H 1560 3260 50  0001 C CNN
	1    1560 3260
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 5A80ACC6
P 840 3470
F 0 "#PWR06" H 840 3320 50  0001 C CNN
F 1 "VCC" H 840 3620 50  0000 C CNN
F 2 "" H 840 3470 50  0001 C CNN
F 3 "" H 840 3470 50  0001 C CNN
	1    840  3470
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2650 6200 2650
Wire Wire Line
	6200 2650 7000 2650
Wire Wire Line
	7000 2650 7800 2650
Wire Wire Line
	7800 2650 8600 2650
Wire Wire Line
	8600 2650 9400 2650
Wire Wire Line
	4600 2650 5400 2650
$Comp
L Device:R R3
U 1 1 5FD9FA87
P 1470 4320
F 0 "R3" H 1540 4366 50  0000 L CNN
F 1 "10k" H 1540 4275 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 1400 4320 50  0001 C CNN
F 3 "~" H 1470 4320 50  0001 C CNN
	1    1470 4320
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 5FDA25A8
P 1470 4110
F 0 "#PWR07" H 1470 3960 50  0001 C CNN
F 1 "VCC" H 1485 4283 50  0000 C CNN
F 2 "" H 1470 4110 50  0001 C CNN
F 3 "" H 1470 4110 50  0001 C CNN
	1    1470 4110
	1    0    0    -1  
$EndComp
$Comp
L icydox:SW_PUSH RST_SW1
U 1 1 5A808917
P 1850 4530
F 0 "RST_SW1" H 2000 4640 50  0000 C CNN
F 1 "SW_PUSH" H 1850 4450 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_PUSH_6mm_h4.3mm" H 1850 4530 60  0001 C CNN
F 3 "" H 1850 4530 60  0000 C CNN
	1    1850 4530
	1    0    0    -1  
$EndComp
Wire Wire Line
	1360 4530 1470 4530
$Comp
L Device:C C1
U 1 1 5FDA0C45
P 1470 4740
F 0 "C1" H 1585 4786 50  0000 L CNN
F 1 "0.1uF" H 1585 4695 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1508 4590 50  0001 C CNN
F 3 "~" H 1470 4740 50  0001 C CNN
	1    1470 4740
	1    0    0    -1  
$EndComp
Wire Wire Line
	1470 4890 1470 4990
$Comp
L power:GND #PWR09
U 1 1 5FDA1868
P 1470 4990
F 0 "#PWR09" H 1470 4740 50  0001 C CNN
F 1 "GND" H 1475 4817 50  0000 C CNN
F 2 "" H 1470 4990 50  0001 C CNN
F 3 "" H 1470 4990 50  0001 C CNN
	1    1470 4990
	1    0    0    -1  
$EndComp
Wire Wire Line
	1470 4590 1470 4530
Connection ~ 1470 4530
Wire Wire Line
	1470 4530 1550 4530
Wire Wire Line
	1470 4530 1470 4470
Wire Wire Line
	1470 4170 1470 4110
$Comp
L Device:Rotary_Encoder_Switch SW1
U 1 1 5FE2438C
P 2120 6020
F 0 "SW1" H 2120 6295 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 2120 6296 50  0001 C CNN
F 2 "Keebio-Parts:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 1970 6180 50  0001 C CNN
F 3 "~" H 2120 6280 50  0001 C CNN
	1    2120 6020
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5FE259B8
P 1550 5710
F 0 "R5" H 1620 5756 50  0000 L CNN
F 1 "10k" H 1620 5665 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 1480 5710 50  0001 C CNN
F 3 "~" H 1550 5710 50  0001 C CNN
	1    1550 5710
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5FE26681
P 1230 5710
F 0 "R4" H 1300 5756 50  0000 L CNN
F 1 "10k" H 1300 5665 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 1160 5710 50  0001 C CNN
F 3 "~" H 1230 5710 50  0001 C CNN
	1    1230 5710
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5FE272EB
P 2580 5920
F 0 "R6" H 2650 5966 50  0000 L CNN
F 1 "10k" H 2650 5875 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 2510 5920 50  0001 C CNN
F 3 "~" H 2580 5920 50  0001 C CNN
	1    2580 5920
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FE27FD4
P 1230 6380
F 0 "C3" H 1345 6426 50  0000 L CNN
F 1 "0.1uF" H 1345 6335 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1268 6230 50  0001 C CNN
F 3 "~" H 1230 6380 50  0001 C CNN
	1    1230 6380
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FE2924C
P 1680 6390
F 0 "C4" H 1565 6344 50  0000 R CNN
F 1 "0.1uF" H 1565 6435 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1718 6240 50  0001 C CNN
F 3 "~" H 1680 6390 50  0001 C CNN
	1    1680 6390
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5FE29FC1
P 2580 6320
F 0 "C2" H 2695 6366 50  0000 L CNN
F 1 "0.1uF" H 2695 6275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2618 6170 50  0001 C CNN
F 3 "~" H 2580 6320 50  0001 C CNN
	1    2580 6320
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5FE2BB0A
P 2580 6560
F 0 "#PWR012" H 2580 6310 50  0001 C CNN
F 1 "GND" H 2585 6387 50  0000 C CNN
F 2 "" H 2580 6560 50  0001 C CNN
F 3 "" H 2580 6560 50  0001 C CNN
	1    2580 6560
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR011
U 1 1 5FE2C36E
P 2580 5710
F 0 "#PWR011" H 2580 5560 50  0001 C CNN
F 1 "VCC" H 2595 5883 50  0000 C CNN
F 2 "" H 2580 5710 50  0001 C CNN
F 3 "" H 2580 5710 50  0001 C CNN
	1    2580 5710
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR010
U 1 1 5FE2CF95
P 1390 5500
F 0 "#PWR010" H 1390 5350 50  0001 C CNN
F 1 "VCC" H 1405 5673 50  0000 C CNN
F 2 "" H 1390 5500 50  0001 C CNN
F 3 "" H 1390 5500 50  0001 C CNN
	1    1390 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1460 6610 1460 6550
Wire Wire Line
	1680 6550 1680 6540
Wire Wire Line
	1230 6530 1230 6550
Wire Wire Line
	1230 6550 1460 6550
Connection ~ 1460 6550
Wire Wire Line
	1460 6550 1680 6550
Wire Wire Line
	1680 6240 1680 6120
Wire Wire Line
	1680 6120 1820 6120
Wire Wire Line
	1820 5920 1230 5920
Wire Wire Line
	1230 5920 1230 6230
Wire Wire Line
	2580 6120 2580 6170
Wire Wire Line
	1820 6020 1460 6020
Wire Wire Line
	1460 6020 1460 6550
Text GLabel 2660 6120 2    60   Input ~ 0
enc_sw
Text GLabel 1160 5920 0    60   Input ~ 0
enc_a
Wire Wire Line
	2660 6120 2580 6120
Connection ~ 2580 6120
Wire Wire Line
	2580 6070 2580 6120
Wire Wire Line
	2580 6120 2580 6130
Wire Wire Line
	1160 5920 1230 5920
Connection ~ 1230 5920
Text GLabel 1160 6120 0    60   Input ~ 0
enc_b
Wire Wire Line
	1160 6120 1550 6120
Connection ~ 1680 6120
Wire Wire Line
	1230 5860 1230 5920
Wire Wire Line
	1550 5860 1550 6120
Connection ~ 1550 6120
Wire Wire Line
	1550 6120 1680 6120
Wire Wire Line
	1230 5560 1230 5530
Wire Wire Line
	1230 5530 1390 5530
Wire Wire Line
	1550 5530 1550 5560
Wire Wire Line
	1390 5500 1390 5530
Connection ~ 1390 5530
Wire Wire Line
	1390 5530 1550 5530
Text GLabel 1450 1700 0    60   Input ~ 0
enc_b
Wire Wire Line
	1450 1700 1650 1700
Wire Wire Line
	2580 6470 2580 6510
Wire Wire Line
	2420 6120 2580 6120
Wire Wire Line
	2420 5920 2470 5920
Wire Wire Line
	2470 5920 2470 6510
Wire Wire Line
	2470 6510 2580 6510
Connection ~ 2580 6510
Wire Wire Line
	2580 6510 2580 6560
$Comp
L icydox:Audio-Jack-4 TRRS1
U 1 1 5A8087E2
P 1150 3350
F 0 "TRRS1" H 1100 3430 50  0000 C CNN
F 1 "Audio-Jack-4" H 1270 2980 50  0000 C CNN
F 2 "Keebio-Parts:TRRS-PJ-320A-dual" H 1400 3450 50  0001 C CNN
F 3 "" H 1400 3450 50  0001 C CNN
	1    1150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	840  3750 950  3750
Wire Wire Line
	1430 3630 1430 3350
Wire Wire Line
	1430 3350 1350 3350
Wire Wire Line
	1350 3450 1560 3450
Wire Wire Line
	1560 3450 1560 3410
Wire Wire Line
	1760 3410 1760 3550
Wire Wire Line
	1760 3550 1350 3550
Wire Wire Line
	1850 3550 1760 3550
Connection ~ 1760 3550
Wire Wire Line
	1850 3450 1560 3450
Connection ~ 1560 3450
$Comp
L power:VCC #PWR0101
U 1 1 5FFCFBD4
P 1660 3050
F 0 "#PWR0101" H 1660 2900 50  0001 C CNN
F 1 "VCC" H 1675 3223 50  0000 C CNN
F 2 "" H 1660 3050 50  0001 C CNN
F 3 "" H 1660 3050 50  0001 C CNN
	1    1660 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1760 3110 1760 3050
Wire Wire Line
	1760 3050 1660 3050
Wire Wire Line
	1560 3110 1560 3050
Wire Wire Line
	1560 3050 1660 3050
Connection ~ 1660 3050
$Comp
L Transistor_FET:DMN6140L Q1
U 1 1 5FE276F3
P 3090 3760
F 0 "Q1" H 3294 3806 50  0000 L CNN
F 1 "DMN6140L" H 3294 3715 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3290 3685 50  0001 L CIN
F 3 "http://www.diodes.com/assets/Datasheets/DMN6140L.pdf" H 3090 3760 50  0001 L CNN
	1    3090 3760
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5FE36F83
P 2780 3970
F 0 "R8" H 2850 4016 50  0000 L CNN
F 1 "10k" H 2850 3925 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 2710 3970 50  0001 C CNN
F 3 "~" H 2780 3970 50  0001 C CNN
	1    2780 3970
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5FE380BD
P 2550 3760
F 0 "R7" H 2620 3806 50  0000 L CNN
F 1 "100" H 2620 3715 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 2480 3760 50  0001 C CNN
F 3 "~" H 2550 3760 50  0001 C CNN
	1    2550 3760
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FE38B8D
P 3190 4010
F 0 "#PWR0102" H 3190 3760 50  0001 C CNN
F 1 "GND" H 3195 3837 50  0000 C CNN
F 2 "" H 3190 4010 50  0001 C CNN
F 3 "" H 3190 4010 50  0001 C CNN
	1    3190 4010
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5FE3A222
P 2780 4180
F 0 "#PWR0103" H 2780 3930 50  0001 C CNN
F 1 "GND" H 2785 4007 50  0000 C CNN
F 2 "" H 2780 4180 50  0001 C CNN
F 3 "" H 2780 4180 50  0001 C CNN
	1    2780 4180
	1    0    0    -1  
$EndComp
Text GLabel 2300 3760 0    60   Input ~ 0
PWM
Text GLabel 3190 3480 1    60   Input ~ 0
DRAIN
Wire Wire Line
	3190 3480 3190 3560
Wire Wire Line
	2400 3760 2300 3760
Wire Wire Line
	2780 3820 2780 3760
Wire Wire Line
	2700 3760 2780 3760
Connection ~ 2780 3760
Wire Wire Line
	2780 3760 2890 3760
Wire Wire Line
	2780 4180 2780 4120
Text GLabel 1450 2300 0    60   Input ~ 0
PWM
Wire Wire Line
	1450 2300 1650 2300
Text GLabel 3300 2000 2    60   Input ~ 0
enc_a
Text GLabel 1450 1600 0    60   Input ~ 0
enc_sw
$Comp
L power:VCC #PWR0104
U 1 1 602BD008
P 4120 710
F 0 "#PWR0104" H 4120 560 50  0001 C CNN
F 1 "VCC" H 4135 883 50  0000 C CNN
F 2 "" H 4120 710 50  0001 C CNN
F 3 "" H 4120 710 50  0001 C CNN
	1    4120 710 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1200 4900 980 
Wire Wire Line
	9700 970  9700 1200
Wire Wire Line
	4900 970  5700 970 
Wire Wire Line
	5700 1200 5700 970 
Connection ~ 5700 970 
Connection ~ 4900 980 
Wire Wire Line
	4900 980  4900 970 
Wire Wire Line
	4600 4970 5400 4970
Wire Wire Line
	8600 4970 9400 4970
Wire Wire Line
	7800 4970 8600 4970
Wire Wire Line
	7000 4970 7800 4970
Wire Wire Line
	6200 4970 7000 4970
Wire Wire Line
	5400 4970 6200 4970
Text GLabel 4500 4970 0    60   Input ~ 0
row4
Text GLabel 4500 4170 0    60   Input ~ 0
row3
Connection ~ 4600 4970
Connection ~ 4600 4170
Connection ~ 8600 4970
Connection ~ 7800 4970
Connection ~ 7000 4970
Connection ~ 6200 4970
Connection ~ 5400 4970
Wire Wire Line
	4500 4970 4600 4970
Wire Wire Line
	9400 4570 9400 4670
$Comp
L Device:D D35
U 1 1 5A80E4E7
P 9400 4820
F 0 "D35" H 9400 4920 50  0000 C CNN
F 1 "D" H 9400 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 9400 4820 50  0001 C CNN
F 3 "" H 9400 4820 50  0001 C CNN
	1    9400 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K35
U 1 1 5A80E4E1
P 9700 4570
F 0 "K35" H 9650 4570 60  0000 C CNN
F 1 "KEYSW" H 9700 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 9700 4570 60  0001 C CNN
F 3 "" H 9700 4570 60  0000 C CNN
	1    9700 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 4570 8600 4670
$Comp
L Device:D D34
U 1 1 5A80E4DA
P 8600 4820
F 0 "D34" H 8600 4920 50  0000 C CNN
F 1 "D" H 8600 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 8600 4820 50  0001 C CNN
F 3 "" H 8600 4820 50  0001 C CNN
	1    8600 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K34
U 1 1 5A80E4D4
P 8900 4570
F 0 "K34" H 8850 4570 60  0000 C CNN
F 1 "KEYSW" H 8900 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8900 4570 60  0001 C CNN
F 3 "" H 8900 4570 60  0000 C CNN
	1    8900 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4570 7800 4670
$Comp
L Device:D D33
U 1 1 5A80E4CD
P 7800 4820
F 0 "D33" H 7800 4920 50  0000 C CNN
F 1 "D" H 7800 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7800 4820 50  0001 C CNN
F 3 "" H 7800 4820 50  0001 C CNN
	1    7800 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K33
U 1 1 5A80E4C7
P 8100 4570
F 0 "K33" H 8050 4570 60  0000 C CNN
F 1 "KEYSW" H 8100 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8100 4570 60  0001 C CNN
F 3 "" H 8100 4570 60  0000 C CNN
	1    8100 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4570 7000 4670
$Comp
L Device:D D32
U 1 1 5A80E4C0
P 7000 4820
F 0 "D32" H 7000 4920 50  0000 C CNN
F 1 "D" H 7000 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7000 4820 50  0001 C CNN
F 3 "" H 7000 4820 50  0001 C CNN
	1    7000 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K32
U 1 1 5A80E4BA
P 7300 4570
F 0 "K32" H 7250 4570 60  0000 C CNN
F 1 "KEYSW" H 7300 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 7300 4570 60  0001 C CNN
F 3 "" H 7300 4570 60  0000 C CNN
	1    7300 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4570 6200 4670
$Comp
L Device:D D31
U 1 1 5A80E4B3
P 6200 4820
F 0 "D31" H 6200 4920 50  0000 C CNN
F 1 "D" H 6200 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 6200 4820 50  0001 C CNN
F 3 "" H 6200 4820 50  0001 C CNN
	1    6200 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K31
U 1 1 5A80E4AD
P 6500 4570
F 0 "K31" H 6450 4570 60  0000 C CNN
F 1 "KEYSW" H 6500 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 6500 4570 60  0001 C CNN
F 3 "" H 6500 4570 60  0000 C CNN
	1    6500 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4570 5400 4670
$Comp
L Device:D D30
U 1 1 5A80E4A6
P 5400 4820
F 0 "D30" H 5400 4920 50  0000 C CNN
F 1 "D" H 5400 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 5400 4820 50  0001 C CNN
F 3 "" H 5400 4820 50  0001 C CNN
	1    5400 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K30
U 1 1 5A80E4A0
P 5700 4570
F 0 "K30" H 5650 4570 60  0000 C CNN
F 1 "KEYSW" H 5700 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 5700 4570 60  0001 C CNN
F 3 "" H 5700 4570 60  0000 C CNN
	1    5700 4570
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4570 4600 4670
$Comp
L Device:D D29
U 1 1 5A80E499
P 4600 4820
F 0 "D29" H 4600 4920 50  0000 C CNN
F 1 "D" H 4600 4720 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 4600 4820 50  0001 C CNN
F 3 "" H 4600 4820 50  0001 C CNN
	1    4600 4820
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K29
U 1 1 5A80E493
P 4900 4570
F 0 "K29" H 4850 4570 60  0000 C CNN
F 1 "KEYSW" H 4900 4470 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 4900 4570 60  0001 C CNN
F 3 "" H 4900 4570 60  0000 C CNN
	1    4900 4570
	1    0    0    -1  
$EndComp
Connection ~ 7800 4170
Connection ~ 7000 4170
Connection ~ 6200 4170
Connection ~ 5400 4170
Wire Wire Line
	4500 4170 4600 4170
$Comp
L Device:D D27
U 1 1 5A80AC32
P 8600 4020
F 0 "D27" H 8600 4120 50  0000 C CNN
F 1 "D" H 8600 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 8600 4020 50  0001 C CNN
F 3 "" H 8600 4020 50  0001 C CNN
	1    8600 4020
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D26
U 1 1 5A80AC25
P 7800 4020
F 0 "D26" H 7800 4120 50  0000 C CNN
F 1 "D" H 7800 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7800 4020 50  0001 C CNN
F 3 "" H 7800 4020 50  0001 C CNN
	1    7800 4020
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D25
U 1 1 5A80AC18
P 7000 4020
F 0 "D25" H 7000 4120 50  0000 C CNN
F 1 "D" H 7000 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7000 4020 50  0001 C CNN
F 3 "" H 7000 4020 50  0001 C CNN
	1    7000 4020
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D24
U 1 1 5A80AC0B
P 6200 4020
F 0 "D24" H 6200 4120 50  0000 C CNN
F 1 "D" H 6200 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 6200 4020 50  0001 C CNN
F 3 "" H 6200 4020 50  0001 C CNN
	1    6200 4020
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D23
U 1 1 5A80ABFE
P 5400 4020
F 0 "D23" H 5400 4120 50  0000 C CNN
F 1 "D" H 5400 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 5400 4020 50  0001 C CNN
F 3 "" H 5400 4020 50  0001 C CNN
	1    5400 4020
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D22
U 1 1 5A80ABF1
P 4600 4020
F 0 "D22" H 4600 4120 50  0000 C CNN
F 1 "D" H 4600 3920 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 4600 4020 50  0001 C CNN
F 3 "" H 4600 4020 50  0001 C CNN
	1    4600 4020
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8600 3770 8600 3870
Wire Wire Line
	7800 4170 8600 4170
Wire Wire Line
	7800 3770 7800 3870
Wire Wire Line
	7000 4170 7800 4170
Wire Wire Line
	7000 3770 7000 3870
Wire Wire Line
	6200 4170 7000 4170
Wire Wire Line
	6200 3770 6200 3870
Wire Wire Line
	5400 4170 6200 4170
Wire Wire Line
	5400 3770 5400 3870
Wire Wire Line
	4600 4170 5400 4170
Wire Wire Line
	4600 3770 4600 3870
Wire Wire Line
	4600 3420 5400 3420
Wire Wire Line
	7800 3420 8600 3420
Wire Wire Line
	7000 3420 7800 3420
Wire Wire Line
	6200 3420 7000 3420
Wire Wire Line
	5400 3420 6200 3420
Text GLabel 4500 3420 0    60   Input ~ 0
row2
Connection ~ 4600 3420
$Comp
L icydox:KEYSW_w_LED K27
U 1 1 5A80AC2C
P 8900 3770
F 0 "K27" H 8850 3770 60  0000 C CNN
F 1 "KEYSW" H 8900 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8900 3770 60  0001 C CNN
F 3 "" H 8900 3770 60  0000 C CNN
	1    8900 3770
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K26
U 1 1 5A80AC1F
P 8100 3770
F 0 "K26" H 8050 3770 60  0000 C CNN
F 1 "KEYSW" H 8100 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8100 3770 60  0001 C CNN
F 3 "" H 8100 3770 60  0000 C CNN
	1    8100 3770
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K25
U 1 1 5A80AC12
P 7300 3770
F 0 "K25" H 7250 3770 60  0000 C CNN
F 1 "KEYSW" H 7300 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 7300 3770 60  0001 C CNN
F 3 "" H 7300 3770 60  0000 C CNN
	1    7300 3770
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K24
U 1 1 5A80AC05
P 6500 3770
F 0 "K24" H 6450 3770 60  0000 C CNN
F 1 "KEYSW" H 6500 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 6500 3770 60  0001 C CNN
F 3 "" H 6500 3770 60  0000 C CNN
	1    6500 3770
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K23
U 1 1 5A80ABF8
P 5700 3770
F 0 "K23" H 5650 3770 60  0000 C CNN
F 1 "KEYSW" H 5700 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 5700 3770 60  0001 C CNN
F 3 "" H 5700 3770 60  0000 C CNN
	1    5700 3770
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K22
U 1 1 5A80ABEB
P 4900 3770
F 0 "K22" H 4850 3770 60  0000 C CNN
F 1 "KEYSW" H 4900 3670 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 4900 3770 60  0001 C CNN
F 3 "" H 4900 3770 60  0000 C CNN
	1    4900 3770
	1    0    0    -1  
$EndComp
Connection ~ 7800 3420
Connection ~ 7000 3420
Connection ~ 6200 3420
Connection ~ 5400 3420
Wire Wire Line
	4500 3420 4600 3420
Wire Wire Line
	8600 3020 8600 3120
$Comp
L Device:D D20
U 1 1 5A80ABD1
P 8600 3270
F 0 "D20" H 8600 3370 50  0000 C CNN
F 1 "D" H 8600 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 8600 3270 50  0001 C CNN
F 3 "" H 8600 3270 50  0001 C CNN
	1    8600 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K20
U 1 1 5A80ABCB
P 8900 3020
F 0 "K20" H 8850 3020 60  0000 C CNN
F 1 "KEYSW" H 8900 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8900 3020 60  0001 C CNN
F 3 "" H 8900 3020 60  0000 C CNN
	1    8900 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3020 7800 3120
$Comp
L Device:D D19
U 1 1 5A80ABC4
P 7800 3270
F 0 "D19" H 7800 3370 50  0000 C CNN
F 1 "D" H 7800 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7800 3270 50  0001 C CNN
F 3 "" H 7800 3270 50  0001 C CNN
	1    7800 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K19
U 1 1 5A80ABBE
P 8100 3020
F 0 "K19" H 8050 3020 60  0000 C CNN
F 1 "KEYSW" H 8100 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8100 3020 60  0001 C CNN
F 3 "" H 8100 3020 60  0000 C CNN
	1    8100 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3020 7000 3120
$Comp
L Device:D D18
U 1 1 5A80ABB7
P 7000 3270
F 0 "D18" H 7000 3370 50  0000 C CNN
F 1 "D" H 7000 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7000 3270 50  0001 C CNN
F 3 "" H 7000 3270 50  0001 C CNN
	1    7000 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K18
U 1 1 5A80ABB1
P 7300 3020
F 0 "K18" H 7250 3020 60  0000 C CNN
F 1 "KEYSW" H 7300 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 7300 3020 60  0001 C CNN
F 3 "" H 7300 3020 60  0000 C CNN
	1    7300 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 3020 6200 3120
$Comp
L Device:D D17
U 1 1 5A80ABAA
P 6200 3270
F 0 "D17" H 6200 3370 50  0000 C CNN
F 1 "D" H 6200 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 6200 3270 50  0001 C CNN
F 3 "" H 6200 3270 50  0001 C CNN
	1    6200 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K17
U 1 1 5A80ABA4
P 6500 3020
F 0 "K17" H 6450 3020 60  0000 C CNN
F 1 "KEYSW" H 6500 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 6500 3020 60  0001 C CNN
F 3 "" H 6500 3020 60  0000 C CNN
	1    6500 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3020 5400 3120
$Comp
L Device:D D16
U 1 1 5A80AB9D
P 5400 3270
F 0 "D16" H 5400 3370 50  0000 C CNN
F 1 "D" H 5400 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 5400 3270 50  0001 C CNN
F 3 "" H 5400 3270 50  0001 C CNN
	1    5400 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K16
U 1 1 5A80AB97
P 5700 3020
F 0 "K16" H 5650 3020 60  0000 C CNN
F 1 "KEYSW" H 5700 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 5700 3020 60  0001 C CNN
F 3 "" H 5700 3020 60  0000 C CNN
	1    5700 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3020 4600 3120
$Comp
L Device:D D15
U 1 1 5A80AB90
P 4600 3270
F 0 "D15" H 4600 3370 50  0000 C CNN
F 1 "D" H 4600 3170 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 4600 3270 50  0001 C CNN
F 3 "" H 4600 3270 50  0001 C CNN
	1    4600 3270
	0    -1   -1   0   
$EndComp
$Comp
L icydox:KEYSW_w_LED K15
U 1 1 5A80AB8A
P 4900 3020
F 0 "K15" H 4850 3020 60  0000 C CNN
F 1 "KEYSW" H 4900 2920 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 4900 3020 60  0001 C CNN
F 3 "" H 4900 3020 60  0000 C CNN
	1    4900 3020
	1    0    0    -1  
$EndComp
Wire Wire Line
	4120 2720 4120 3470
Wire Wire Line
	4120 4270 4120 3470
Connection ~ 4120 3470
Text GLabel 10130 1840 2    60   Input ~ 0
DRAIN
Wire Wire Line
	10130 1850 10130 1840
Wire Wire Line
	10110 2600 10110 1850
Wire Wire Line
	10110 1850 10130 1850
Connection ~ 10110 2600
Wire Wire Line
	10110 2600 10110 3370
Connection ~ 10110 3370
Wire Wire Line
	10110 3370 10110 4120
Wire Wire Line
	10110 4920 10110 4120
Connection ~ 10110 4120
Wire Wire Line
	6500 1200 6500 970 
Wire Wire Line
	5700 970  6500 970 
Connection ~ 6500 970 
Wire Wire Line
	8100 1200 8100 970 
Connection ~ 8100 970 
Wire Wire Line
	6500 970  7300 970 
Wire Wire Line
	7300 1200 7300 970 
Connection ~ 7300 970 
Wire Wire Line
	7300 970  8100 970 
Connection ~ 8900 4120
Wire Wire Line
	8900 4120 10110 4120
Connection ~ 8900 4270
Wire Wire Line
	8900 4270 9700 4270
Connection ~ 8900 4920
Wire Wire Line
	8900 4920 9700 4920
Connection ~ 9700 4920
Wire Wire Line
	9700 4920 10110 4920
Connection ~ 8100 4920
Wire Wire Line
	8100 4920 8900 4920
Connection ~ 7300 4920
Wire Wire Line
	7300 4920 8100 4920
Wire Wire Line
	6500 4920 7300 4920
Wire Wire Line
	4900 4920 5700 4920
$Comp
L icydox:KEYSW_w_LED K1
U 1 1 5A808C37
P 4900 1500
F 0 "K1" H 4850 1500 60  0000 C CNN
F 1 "KEYSW" H 4900 1400 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 4900 1500 60  0001 C CNN
F 3 "" H 4900 1500 60  0000 C CNN
	1    4900 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10110 1850 10110 1860
Connection ~ 10110 1850
Wire Wire Line
	5700 1850 6500 1850
Connection ~ 6500 1850
Wire Wire Line
	6500 1850 7300 1850
Connection ~ 7300 1850
Wire Wire Line
	7300 1850 8100 1850
Connection ~ 8100 1850
Wire Wire Line
	8100 1850 8900 1850
Connection ~ 8900 1850
Wire Wire Line
	8900 1850 9700 1850
Connection ~ 9700 1850
Wire Wire Line
	9700 1850 10110 1850
Wire Wire Line
	4900 1850 5700 1850
Connection ~ 5700 1850
Wire Wire Line
	5200 1300 5200 1500
Wire Wire Line
	6000 1300 6000 1500
Wire Wire Line
	6800 1300 6800 1500
Wire Wire Line
	7600 1300 7600 1500
Wire Wire Line
	8400 1300 8400 1500
Wire Wire Line
	9200 1350 9200 1500
Connection ~ 9200 1500
Connection ~ 8400 1500
Connection ~ 7600 1500
Connection ~ 6800 1500
Connection ~ 6000 1500
Connection ~ 5200 1500
Connection ~ 4120 2720
Wire Wire Line
	4120 980  4900 980 
Wire Wire Line
	4120 1950 4120 2720
Connection ~ 4120 1950
Wire Wire Line
	4120 1950 4120 980 
Wire Wire Line
	4600 1900 5400 1900
Wire Wire Line
	8600 1900 9400 1900
Wire Wire Line
	7800 1900 8600 1900
Wire Wire Line
	7000 1900 7800 1900
Wire Wire Line
	6200 1900 7000 1900
Wire Wire Line
	5400 1900 6200 1900
Text GLabel 4500 1900 0    60   Input ~ 0
row0
Connection ~ 4600 1900
$Comp
L icydox:KEYSW_w_LED K14
U 1 1 5A809C6B
P 9700 2250
F 0 "K14" H 9650 2250 60  0000 C CNN
F 1 "KEYSW" H 9700 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 9700 2250 60  0001 C CNN
F 3 "" H 9700 2250 60  0000 C CNN
	1    9700 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K13
U 1 1 5A809C5E
P 8900 2250
F 0 "K13" H 8850 2250 60  0000 C CNN
F 1 "KEYSW" H 8900 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8900 2250 60  0001 C CNN
F 3 "" H 8900 2250 60  0000 C CNN
	1    8900 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K12
U 1 1 5A809C51
P 8100 2250
F 0 "K12" H 8050 2250 60  0000 C CNN
F 1 "KEYSW" H 8100 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 8100 2250 60  0001 C CNN
F 3 "" H 8100 2250 60  0000 C CNN
	1    8100 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K11
U 1 1 5A809C44
P 7300 2250
F 0 "K11" H 7250 2250 60  0000 C CNN
F 1 "KEYSW" H 7300 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 7300 2250 60  0001 C CNN
F 3 "" H 7300 2250 60  0000 C CNN
	1    7300 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K10
U 1 1 5A809C37
P 6500 2250
F 0 "K10" H 6450 2250 60  0000 C CNN
F 1 "KEYSW" H 6500 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 6500 2250 60  0001 C CNN
F 3 "" H 6500 2250 60  0000 C CNN
	1    6500 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K9
U 1 1 5A809C2A
P 5700 2250
F 0 "K9" H 5650 2250 60  0000 C CNN
F 1 "KEYSW" H 5700 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 5700 2250 60  0001 C CNN
F 3 "" H 5700 2250 60  0000 C CNN
	1    5700 2250
	1    0    0    -1  
$EndComp
$Comp
L icydox:KEYSW_w_LED K8
U 1 1 5A809C1D
P 4900 2250
F 0 "K8" H 4850 2250 60  0000 C CNN
F 1 "KEYSW" H 4900 2150 60  0001 C CNN
F 2 "Keebio-Parts:Hybrid_PCB_100H_Dual_hole-nosilk" H 4900 2250 60  0001 C CNN
F 3 "" H 4900 2250 60  0000 C CNN
	1    4900 2250
	1    0    0    -1  
$EndComp
Connection ~ 8600 1900
Connection ~ 7800 1900
Connection ~ 7000 1900
Connection ~ 6200 1900
Connection ~ 5400 1900
Wire Wire Line
	4500 1900 4600 1900
$Comp
L Device:D D7
U 1 1 5A8094AD
P 9400 1750
F 0 "D7" H 9400 1850 50  0000 C CNN
F 1 "D" H 9400 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 9400 1750 50  0001 C CNN
F 3 "" H 9400 1750 50  0001 C CNN
	1    9400 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D6
U 1 1 5A8094A0
P 8600 1750
F 0 "D6" H 8600 1850 50  0000 C CNN
F 1 "D" H 8600 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 8600 1750 50  0001 C CNN
F 3 "" H 8600 1750 50  0001 C CNN
	1    8600 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D5
U 1 1 5A809493
P 7800 1750
F 0 "D5" H 7800 1850 50  0000 C CNN
F 1 "D" H 7800 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7800 1750 50  0001 C CNN
F 3 "" H 7800 1750 50  0001 C CNN
	1    7800 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D4
U 1 1 5A809209
P 7000 1750
F 0 "D4" H 7000 1850 50  0000 C CNN
F 1 "D" H 7000 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 7000 1750 50  0001 C CNN
F 3 "" H 7000 1750 50  0001 C CNN
	1    7000 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D3
U 1 1 5A8091FC
P 6200 1750
F 0 "D3" H 6200 1850 50  0000 C CNN
F 1 "D" H 6200 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 6200 1750 50  0001 C CNN
F 3 "" H 6200 1750 50  0001 C CNN
	1    6200 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D2
U 1 1 5A80908F
P 5400 1750
F 0 "D2" H 5400 1850 50  0000 C CNN
F 1 "D" H 5400 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 5400 1750 50  0001 C CNN
F 3 "" H 5400 1750 50  0001 C CNN
	1    5400 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D1
U 1 1 5A808D18
P 4600 1750
F 0 "D1" H 4600 1850 50  0000 C CNN
F 1 "D" H 4600 1650 50  0000 C CNN
F 2 "Keebio-Parts:Diode-dual" H 4600 1750 50  0001 C CNN
F 3 "" H 4600 1750 50  0001 C CNN
	1    4600 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10000 1350 10000 1500
Connection ~ 10000 1500
Connection ~ 8900 1950
Wire Wire Line
	8900 1950 9700 1950
Connection ~ 8100 1950
Wire Wire Line
	8100 1950 8900 1950
Wire Wire Line
	10000 1500 10000 2250
Connection ~ 10000 2250
Wire Wire Line
	10000 2250 10000 4570
Connection ~ 7300 1950
Wire Wire Line
	7300 1950 8100 1950
Connection ~ 6500 1950
Wire Wire Line
	6500 1950 7300 1950
Connection ~ 5700 1950
Wire Wire Line
	5700 1950 6500 1950
Wire Wire Line
	4120 1950 4900 1950
Connection ~ 4900 1950
Wire Wire Line
	4900 1950 5700 1950
Wire Wire Line
	5200 1500 5200 2250
Connection ~ 5200 2250
Wire Wire Line
	6000 1500 6000 2250
Connection ~ 6000 2250
Wire Wire Line
	6800 1500 6800 2250
Connection ~ 6800 2250
Wire Wire Line
	7600 1500 7600 2250
Connection ~ 7600 2250
Wire Wire Line
	8400 1500 8400 2250
Connection ~ 8400 2250
Wire Wire Line
	9200 1500 9200 2250
Connection ~ 9200 2250
Wire Wire Line
	4120 2720 4900 2720
Connection ~ 4900 2720
Wire Wire Line
	5200 2250 5200 3020
Connection ~ 5200 3020
Wire Wire Line
	4900 2600 5700 2600
Connection ~ 5700 2600
Wire Wire Line
	4900 2720 5700 2720
Connection ~ 5700 2720
Wire Wire Line
	6000 2250 6000 3020
Connection ~ 6000 3020
Wire Wire Line
	5700 2720 6500 2720
Connection ~ 6500 2720
Wire Wire Line
	5700 2600 6500 2600
Connection ~ 6500 2600
Wire Wire Line
	6800 2250 6800 3020
Connection ~ 6800 3020
Wire Wire Line
	6500 2600 7300 2600
Connection ~ 7300 2600
Wire Wire Line
	6500 2720 7300 2720
Connection ~ 7300 2720
Wire Wire Line
	7300 2720 8100 2720
Connection ~ 8100 2720
Wire Wire Line
	7300 2600 8100 2600
Connection ~ 8100 2600
Wire Wire Line
	7600 2250 7600 3020
Connection ~ 7600 3020
Wire Wire Line
	8400 2250 8400 3020
Connection ~ 8400 3020
Wire Wire Line
	9200 2250 9200 3020
Connection ~ 9200 3020
Wire Wire Line
	8100 2720 8900 2720
Connection ~ 8900 2720
Wire Wire Line
	8900 2720 8910 2720
Wire Wire Line
	8100 2600 8900 2600
Connection ~ 8900 2600
Wire Wire Line
	8900 2600 9700 2600
Connection ~ 9700 2600
Wire Wire Line
	9700 2600 10110 2600
Wire Wire Line
	5200 3020 5200 3770
Connection ~ 5200 3770
Wire Wire Line
	5200 3770 5200 4570
Wire Wire Line
	4120 3470 4900 3470
Connection ~ 4900 3470
Wire Wire Line
	4900 3470 5700 3470
Connection ~ 5700 3470
Wire Wire Line
	4900 3370 5700 3370
Connection ~ 5700 3370
Wire Wire Line
	6000 3020 6000 3770
Connection ~ 6000 3770
Wire Wire Line
	6000 3770 6000 4570
Wire Wire Line
	5700 3470 6500 3470
Connection ~ 6500 3470
Wire Wire Line
	5700 3370 6500 3370
Connection ~ 6500 3370
Wire Wire Line
	6800 3020 6800 3770
Connection ~ 6800 3770
Wire Wire Line
	6800 3770 6800 4570
Wire Wire Line
	6500 3470 7300 3470
Connection ~ 7300 3470
Wire Wire Line
	6500 3370 7300 3370
Connection ~ 7300 3370
Wire Wire Line
	7600 3020 7600 3770
Connection ~ 7600 3770
Wire Wire Line
	7600 3770 7600 4570
Wire Wire Line
	7300 3370 8100 3370
Connection ~ 8100 3370
Wire Wire Line
	7300 3470 8100 3470
Connection ~ 8100 3470
Wire Wire Line
	8100 3470 8900 3470
Wire Wire Line
	8100 3370 8900 3370
Connection ~ 8900 3370
Wire Wire Line
	8900 3370 10110 3370
Wire Wire Line
	9200 3020 9200 3770
Connection ~ 9200 3770
Wire Wire Line
	9200 3770 9200 4570
Wire Wire Line
	4120 4270 4900 4270
Connection ~ 4900 4270
Wire Wire Line
	4900 4270 5700 4270
Connection ~ 5700 4270
Wire Wire Line
	4900 4120 5700 4120
Connection ~ 5700 4120
Wire Wire Line
	5700 4120 6500 4120
Connection ~ 6500 4120
Wire Wire Line
	5700 4270 6500 4270
Connection ~ 6500 4270
Wire Wire Line
	6500 4120 7300 4120
Connection ~ 7300 4120
Wire Wire Line
	6500 4270 7300 4270
Connection ~ 7300 4270
Wire Wire Line
	7300 4270 8100 4270
Connection ~ 8100 4270
Wire Wire Line
	8100 4270 8900 4270
Wire Wire Line
	7300 4120 8100 4120
Connection ~ 8100 4120
Wire Wire Line
	8100 4120 8900 4120
Wire Wire Line
	8400 3020 8400 3770
Connection ~ 8400 3770
Wire Wire Line
	8400 3770 8400 4570
Wire Wire Line
	8100 970  8900 970 
Wire Wire Line
	8900 970  8900 1200
Connection ~ 8900 970 
Wire Wire Line
	8900 970  9700 970 
Connection ~ 8900 1200
Wire Wire Line
	8900 1200 8900 1220
Wire Wire Line
	4120 710  4120 980 
Connection ~ 4120 980 
Wire Wire Line
	840  3750 840  3470
Wire Wire Line
	3190 4010 3190 3960
Wire Wire Line
	2580 5770 2580 5710
Wire Wire Line
	3050 2000 3300 2000
$Comp
L power:GND #PWR0105
U 1 1 60439005
P 1460 6610
F 0 "#PWR0105" H 1460 6360 50  0001 C CNN
F 1 "GND" H 1465 6437 50  0000 C CNN
F 2 "" H 1460 6610 50  0001 C CNN
F 3 "" H 1460 6610 50  0001 C CNN
	1    1460 6610
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60472535
P 1090 7470
F 0 "#FLG0101" H 1090 7545 50  0001 C CNN
F 1 "PWR_FLAG" H 1090 7643 50  0000 C CNN
F 2 "" H 1090 7470 50  0001 C CNN
F 3 "~" H 1090 7470 50  0001 C CNN
	1    1090 7470
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 604732DC
P 1090 7520
F 0 "#PWR0106" H 1090 7270 50  0001 C CNN
F 1 "GND" H 1095 7347 50  0000 C CNN
F 2 "" H 1090 7520 50  0001 C CNN
F 3 "" H 1090 7520 50  0001 C CNN
	1    1090 7520
	1    0    0    -1  
$EndComp
Wire Wire Line
	1090 7520 1090 7470
$Comp
L power:VCC #PWR0107
U 1 1 6049906E
P 730 7430
F 0 "#PWR0107" H 730 7280 50  0001 C CNN
F 1 "VCC" H 745 7603 50  0000 C CNN
F 2 "" H 730 7430 50  0001 C CNN
F 3 "" H 730 7430 50  0001 C CNN
	1    730  7430
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6049A043
P 730 7520
F 0 "#FLG0102" H 730 7595 50  0001 C CNN
F 1 "PWR_FLAG" H 730 7693 50  0000 C CNN
F 2 "" H 730 7520 50  0001 C CNN
F 3 "~" H 730 7520 50  0001 C CNN
	1    730  7520
	-1   0    0    1   
$EndComp
Wire Wire Line
	730  7520 730  7430
Wire Wire Line
	5700 4920 6500 4920
Connection ~ 5700 4920
Connection ~ 6500 4920
$EndSCHEMATC
