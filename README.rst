Icydox keyboard
===============

Keyboard based on Redox `Redox keyboard`_, with custom pcb, case and firmware.

Pcb
---

Based on `Redox's pcb layout v1.0 <https://github.com/mattdibi/redox-keyboard/tree/master/redox/pcb/rev1.0>`_,
with complete modification of routing, footprints, etc, making simpler. Added rotary encoder to scroll,
backlight, and removed some keys.

Case
----

Based on Lenbok_'s Redox rev0b case, and use libraries like `Lenbok's utils <https://github.com/Lenbok/scad-lenbok-utils>`_ and NopSCADlib_.

Firmware
--------

Uses Qmk_ firmware.

.. _Redox keyboard: https://github.com/mattdibi/redox-keyboard
.. _Lenbok: https://github.com/Lenbok/scad-keyboard-cases
.. _NopSCADlib: https://github.com/nophead/NopSCADlib
.. _Qmk: https://qmk.fm
